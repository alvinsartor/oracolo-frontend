﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Assets.Scripts.Extensions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class ContentAdder<T>
{
    [NotNull] private readonly Transform m_TemplateTransform;
    [NotNull] private readonly Func<T, string> m_GetKey;
    [NotNull] private readonly Action<T> m_WhenPressed;
    private readonly Transform m_ContentTransform;
    private readonly GameObject m_ItemTemplate;

    private readonly Dictionary<string, (T, GameObject)> m_Entries;

    public ContentAdder(
        [NotNull] Transform templateTransform,
        [NotNull] Func<T, string> getKey,
        [NotNull] Action<T> whenPressed,
        [NotNull] GameObject itemTemplate)
    {
        m_Entries = new Dictionary<string, (T, GameObject)>();
        m_TemplateTransform = templateTransform;
        m_GetKey = getKey;
        m_WhenPressed = whenPressed;

        m_ContentTransform = templateTransform.Find("Viewport").Find("Content");
        m_ItemTemplate = itemTemplate;
    }

    public void AddOrSubstituteEntries([NotNull] IEnumerable<T> entries)
    {
        var groupedEntries = entries.GroupBy(x => m_Entries.ContainsKey(m_GetKey(x))).ToList();

        var existingEntries = groupedEntries.SingleOrDefault(x => x.Key)?.GetEnumerator();
        var entriesToAdd = groupedEntries.SingleOrDefault(x => !x.Key)?.GetEnumerator();

        DestroyEntriesNotIn(existingEntries);
        AddEntries(entriesToAdd);

        m_TemplateTransform.gameObject.SetActive(m_Entries.Count > 0);
    }

    private void AddEntries([CanBeNull] IEnumerator<T> entriesToAdd)
    {
        foreach (var entry in entriesToAdd?.GetEnumerable() ?? new List<T>())
        {
            string key = m_GetKey(entry);
            GameObject instance = Object.Instantiate(m_ItemTemplate, new Vector3(), new Quaternion());
            instance.GetComponentInChildren<TMPro.TMP_Text>().text = key;
            instance.GetComponent<Toggle>().onValueChanged.AddListener(b => m_WhenPressed(entry));
            instance.transform.SetParent(m_ContentTransform);

            m_Entries.Add(key, (entry, instance));
        }
    }

    private void DestroyEntriesNotIn([CanBeNull] IEnumerator<T> existingEntries)
    {
        ImmutableHashSet<string> existing = (existingEntries?.GetEnumerable() ?? new List<T>()).Select(x => m_GetKey(x)).ToImmutableHashSet();
        ImmutableList<string> toDelete = m_Entries.Keys.Where(x => !existing.Contains(x)).ToImmutableList();

        foreach (var key in toDelete)
        {
            GameObject entry = m_Entries[key].Item2;
            entry.GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
            m_Entries.Remove(key);
            Object.Destroy(entry.gameObject);
        }
    }

    public void Empty()
    {
        DestroyEntriesNotIn(ImmutableList<T>.Empty.GetEnumerator());
        m_TemplateTransform.gameObject.SetActive(false);
    }
}