﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace RuntimeGizmos
{
    public class AxisVectors
    {
        public AxisVectors()
        {
            X = new List<Vector3>();
            Y = new List<Vector3>();
            Z = new List<Vector3>();
            All = new List<Vector3>();
        }

        [NotNull] public List<Vector3> All { get; }

        [NotNull] public List<Vector3> X { get; }

        [NotNull] public List<Vector3> Y { get; }

        [NotNull] public List<Vector3> Z { get; }

        public void Add([NotNull] AxisVectors axisVectors)
        {
            X.AddRange(axisVectors.X);
            Y.AddRange(axisVectors.Y);
            Z.AddRange(axisVectors.Z);
            All.AddRange(axisVectors.All);
        }

        public void Clear()
        {
            X.Clear();
            Y.Clear();
            Z.Clear();
            All.Clear();
        }
    }
}