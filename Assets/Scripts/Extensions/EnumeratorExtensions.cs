﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Assets.Scripts.Extensions
{
    public static class EnumeratorExtensions
    {
        [NotNull]
        public static IEnumerable<T> GetEnumerable<T>([NotNull] this IEnumerator<T> enumerator)
        {
            while (enumerator.MoveNext())
            {
                yield return enumerator.Current;
            }
        }


    }
}
