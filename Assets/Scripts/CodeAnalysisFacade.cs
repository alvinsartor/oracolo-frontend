﻿using System.Collections.Generic;
using System.Collections.Immutable;
using CodeAnalysis.Mapper;
using CodeAnalysis.ProjectMapping.Interfaces;
using JetBrains.Annotations;

public sealed class CodeAnalysisFacade
{   
    [NotNull] private readonly Mapper m_Mapper;

    public CodeAnalysisFacade()
    {
        m_Mapper = new Mapper(Settings.SlnPath);
    }

    public bool VerifyAssemblyName([NotNull] string name) => m_Mapper.CsprojsNames.ContainsKey(name);

    public bool VerifyFileName([NotNull] string name) => m_Mapper.FileNames.Contains(name);

    [NotNull]
    public ICsprojDescriptor GetCsprojByName([NotNull] string name) => m_Mapper.CsprojsNames[name];

    [NotNull]
    public ICsprojDescriptor GetCsprojByPath([NotNull] string path) => m_Mapper.Csprojs[path];

    [NotNull]
    public IEnumerable<ICsprojDescriptor> GetDependenciesOf([NotNull] ICsprojDescriptor csproj) => m_Mapper.DependenciesOf[csproj];

    [NotNull]
    public IEnumerable<ICsprojDescriptor> GetDependentantsOf([NotNull] ICsprojDescriptor csproj) => m_Mapper.DependatsBy[csproj];

    [NotNull]
    public ImmutableDictionary<string, ICsprojDescriptor> AllCsprojs() => m_Mapper.CsprojsNames;

    public int CountDependenciesOf([NotNull] ICsprojDescriptor csproj) => m_Mapper.DependenciesOf[csproj].Count;

    public int CountDependantsOn([NotNull] ICsprojDescriptor csproj) => m_Mapper.DependatsBy[csproj].Count;
}