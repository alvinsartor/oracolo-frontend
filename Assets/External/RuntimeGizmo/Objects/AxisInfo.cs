﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace RuntimeGizmos
{
    public class AxisInfo
    {
        public Vector3 XAxisEnd { get; private set; }
        public Vector3 YAxisEnd { get; private set; }
        public Vector3 ZAxisEnd { get; private set; }
        public Vector3 XDirection { get; private set; }
        public Vector3 YDirection { get; private set; }
        public Vector3 ZDirection { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AxisInfo"/> class.
        /// </summary>
        public AxisInfo()
        {
            XAxisEnd = new Vector3();
            YAxisEnd = new Vector3();
            ZAxisEnd = new Vector3();
            XDirection = new Vector3();
            YDirection = new Vector3();
            ZDirection = new Vector3();
        }

        /// <summary>
        /// Sets the axis info.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="pivot">The pivot.</param>
        /// <param name="handleLength">Length of the handle.</param>
        /// <param name="space">The space.</param>
        /// <exception cref="ArgumentOutOfRangeException">space - null</exception>
        public void Set([NotNull] Transform target, Vector3 pivot, float handleLength, TransformSpace space)
        {
            switch (space)
            {
                case TransformSpace.Global:
                    XDirection = Vector3.right;
                    YDirection = Vector3.up;
                    ZDirection = Vector3.forward;
                    break;
                case TransformSpace.Local:
                    XDirection = target.right;
                    YDirection = target.up;
                    ZDirection = target.forward;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(space), space, null);
            }

            XAxisEnd = pivot + (XDirection * handleLength);
            YAxisEnd = pivot + (YDirection * handleLength);
            ZAxisEnd = pivot + (ZDirection * handleLength);
        }
    }
}