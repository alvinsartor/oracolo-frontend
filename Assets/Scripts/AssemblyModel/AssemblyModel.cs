﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeAnalysis.ProjectMapping.Interfaces;
using Extensions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.UI
{
    public sealed class AssemblyModel : MonoBehaviour
    {
        [SerializeField] private Sprite m_SelctedModelSprite;
        [SerializeField] private Sprite m_DeselctedModelSprite;
        [SerializeField] public GameObject DetailsContainerModel;

        private Image m_ModelImage;

        [CanBeNull] private static AssemblyModel s_SelectedAssemblyModel;

        [NotNull] private GraphBuilder m_Builder;
        [NotNull] private ICsprojDescriptor m_Csproj;

        private HashSet<AssemblyModel> m_ExitingConnections;
        private HashSet<AssemblyModel> m_EnteringConnections;
        
        public void Initialize([NotNull] GraphBuilder builder, [NotNull] ICsprojDescriptor csproj)
        {
            m_Builder = builder;
            Csproj = csproj;

            m_ExitingConnections = new HashSet<AssemblyModel>();
            m_EnteringConnections = new HashSet<AssemblyModel>();

            m_ModelImage = transform.GetComponent<Image>();
            GetComponent<Canvas>().worldCamera = m_Builder.m_MainCamera;

            UpdateUI();
        }

        /// <summary>
        /// Gets the model Csproj.
        /// </summary>
        [NotNull]
        public ICsprojDescriptor Csproj
        {
            get => m_Csproj ?? throw new InvalidOperationException("The Csproj has not been set for this object.");
            private set => m_Csproj = value;
        }

        /// <summary>
        /// Gets the model name.
        /// </summary>
        [NotNull]
        public string Name => Csproj.CsprojName;

        /// <summary>
        /// Gets the model description.
        /// </summary>
        [NotNull]
        public string Description => $"{Csproj.FilePath}\nFiles: {Csproj.FilePath.Length}";

        /// <summary>
        /// Function called when the remove button is pressed.
        /// </summary>
        public void RequestRemotion()
        {
            AssemblyEventSystem.FetchCameraGizmo().RemoveFromTargets(this);
            m_Builder.RemoveAssembly(Csproj);
        }


        /// <summary>
        /// Function called when the expand button is pressed.
        /// </summary>
        public void RequestExpansion()
        {
            m_Builder.ExpandInBlock(this);
        }

        /// <summary>
        /// Function called when the highlight-dependencies or highlight-dependants buttons are pressed.
        /// </summary>
        public void RequestHighlightDependencies(bool exiting)
        {
            m_Builder.SelectiveHighlight(Csproj, exiting, 5);
        }

        private void UpdateUI()
        {
            transform.GetComponent<AssemblyEventSystem>().Initialize(this);
        }

        /// <summary>
        /// Adds a connection to the specified destination.
        /// </summary>
        /// <param name="destination">The connection destination.</param>
        public void AddConnectionFromSource([NotNull] AssemblyModel destination)
        {
            if (IsConnectedTo(destination))
            {
                throw new ArgumentException($"The model is already connected to '{destination.Csproj.CsprojName}'");
            }

            m_ExitingConnections.Add(destination);
            destination.AddConnectionFromDestination(this);
            ConnectionsHandler.Instance.AddConnection(this, destination);
        }

        /// <summary>
        /// Connection addition counterpart.
        /// </summary>
        /// <param name="source">The connection source.</param>
        private void AddConnectionFromDestination([NotNull] AssemblyModel source)
        {
            if (IsConnectedTo(source))
            {
                throw new ArgumentException($"The model is already connected to '{source.Csproj.CsprojName}'");
            }

            m_EnteringConnections.Add(source);
        }

        /// <summary>
        /// Tells if the actual model is (already) connected to the specified one.
        /// </summary>
        /// <param name="model">The model we want to check.</param>
        public bool IsConnectedTo([NotNull] AssemblyModel model)
        {
            return m_EnteringConnections.Contains(model) || m_ExitingConnections.Contains(model);
        }

        /// <summary>
        /// Removes all the connections of the model.
        /// </summary>
        public void RemoveAllConnections()
        {
            m_ExitingConnections.ForEach(destination =>
            {
                destination.m_EnteringConnections.Remove(this);
                ConnectionsHandler.Instance.RemoveConnection(this, destination);
            });
            m_EnteringConnections.ForEach(source =>
            {
                source.m_ExitingConnections.Remove(this);
                ConnectionsHandler.Instance.RemoveConnection(source, this);
            });
        }

        public void StartHighlightConnections()
        {
            m_ExitingConnections.ForEach(destination => ConnectionsHandler.Instance.StartHighlightConnection(this, destination));
            m_EnteringConnections.ForEach(source => ConnectionsHandler.Instance.StartHighlightConnection(source, this));
        }

        public void EndHighlightConnections()
        {
            m_ExitingConnections.ForEach(destination => ConnectionsHandler.Instance.EndHighlightConnection(this, destination));
            m_EnteringConnections.ForEach(source => ConnectionsHandler.Instance.EndHighlightConnection(source, this));
        }

        public void UpdateConnectorLines()
        {
            m_ExitingConnections.ForEach(destination => ConnectionsHandler.Instance.RecalculateConnection(this, destination));
            m_EnteringConnections.ForEach(source => ConnectionsHandler.Instance.RecalculateConnection(source, this));
        }

        public void SetTransparent()
        {
            SetOpacity(Settings.AlphaValueForUnselectedAssembly);
            m_ExitingConnections.ForEach(destination => ConnectionsHandler.Instance.SetTransparent(this, destination));
        }

        public void SetVisible(bool exiting)
        {
            SetOpacity(Settings.AlphaValueForSelectedAssembly);

            if (exiting)
            {
                m_ExitingConnections.ForEach(destination => ConnectionsHandler.Instance.SetVisible(this, destination));
            }
            else
            {
                m_EnteringConnections.ForEach(source => ConnectionsHandler.Instance.SetVisible(source, this));
            }
        }

        private void SetOpacity(float value)
        {
            m_ModelImage.color = m_ModelImage.color.WithAlpha(value);
        }

        public void Select()
        {
            s_SelectedAssemblyModel?.Deselect();

            m_ModelImage.sprite = m_SelctedModelSprite;
            s_SelectedAssemblyModel = this;
        }

        public void Deselect()
        {
            if (m_ModelImage != null)
            {
                m_ModelImage.sprite = m_DeselctedModelSprite;
            }

            s_SelectedAssemblyModel = null;
        }

        public static void DeselectCurrentSelected()
        {
            s_SelectedAssemblyModel?.Deselect();
        }
    }
}